# Dokumentation Übung - Ping mit Switch

 - Datum: 28.01.2022
 - Name: Kaiwen Shao
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors](LAB1.png)



/ip/route/add dst-address=192.168.34.0/24 gateway=192.168.128.2

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
ip 192.168.1.2 255.255.255.0

```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
ip 192.168.1.3 255.255.255.0


```



## Quellen

## Neue Lerninhalte
Microtik
GNS3
VPCS

## Reflexion
Einfach

