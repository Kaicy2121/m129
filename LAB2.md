# Dokumentation Übung - Ping mit Router

 - Datum: 28.01.2022
 - Name: Kaiwen Shao
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors](LAB2.png)


## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
    /system/identity set name=R1
    /ip/address add address=192.168.1.1/24 interface=ether1
    /ip/address add address=192.168.2.1/24 interface=ether2
```

/system/identity set name=R2
/ip/address/add address=192.168.44.2/24 interface=ether2
/ip/address/add address=192.168.128.1/30 interface=ether3


/ip/route/add dst-address=192.168.34.0/24 gateway=192.168.128.2

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
ip 192.168.1.2 255.255.255.0 192.168.1.1

```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC2
ip 192.168.2.2 255.255.255.0 192.168.2.1


```



## Quellen
 - Tim Schefer

## Neue Lerninhalte
Microtik
GNS3
VPCS

## Reflexion
Einfach
