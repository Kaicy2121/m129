# Dokumentation Übung - Ping über Router (3 Subnetze) und lokalem PC 

 - Datum: 28.01.2022
 - Name: Kaiwen Shao
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors](LAB3.png)

## Cloud
 - br0 192.168.23.0/24
 - Eigener PC ist via OpenVPN (Layer2) mit br0 verbunden. 

## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R1
/ip/address/add address=192.168.34.2/24 interface=ether2
/ip/address/add address=192.168.24.2/24 interface=ether3
/ip/address/add address=192.168.23.137/24 interface=ether1

/ip/route/add dst-address=192.168.44.0/24 gateway=192.168.128.1
```
## Config R2
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R2
/ip/address/add address=192.168.44.2/24 interface=ether2
/ip/address/add address=192.168.128.1/30 interface=ether3


/ip/route/add dst-address=192.168.34.0/24 gateway=192.168.128.2

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
ip 192.168.34.1 255.255.255.0 192.168.34.2
```

## Config Eigener Laptop
In *cmd.exe* als Admin:
```cmd
route add 192.168.34.0 mask 255.255.255.0 192.168.23.137
route add 192.168.44.0 mask 255.255.255.0 192.168.23.137

```

## Quellen
 - David Rosenfeld
 - Tim Schefer

## Neue Lerninhalte
 -Routing
 -Lokale Route auf auf einem Windows 10 PC konfigurieren
 -Routen mit Mikrotik Konsole hinzufügen

## Reflexion
Zu beginn war mich mit der Übung komplett Überfordert, und hätte mir etwas mehr, beziehungsweise detailierte erklärungen dazu gewünscht was Routing überhaupt ist, und wie man es auf einem
MikroTik Router konfigurieren kann. Sich das ganze wissen selbst anzueignen war etwas mühsam, aber mittlerweile verstehe ich die Prinzipien und es ist mir gelungen das Labor erfolgreich abzuschliessen.
