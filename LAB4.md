# Dokumentation Übung - Aggregierte statische Routen

 - Datum: 04.02.2022
 - Name: Kaiwen Shao
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors](LAB4.png)

## Cloud
br0 192.168.23.0
Eigener PC ist via OpenVPN (Layer2) mit br0 verbunden.

## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R1
/ip/address add address=192.168.28.1/24 interface=ether2
/ip/address add address=192.168.255.1/30 interface=ether3

/ip/route/ add dst-address=192.168.128.0/17 gateway=192.168.255.2
```
## Config R2
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R2
/ip/address add address=192.168.255.2/30 interface=ether3
/ip/address add address=192.168.146.1/24 interface=ether2
/ip/address add address=192.168.255.5/30 interface=ether4

/ip/route/ add dst-address=192.168.210.0/24 gateway=192.168.255.6
/ip/route/ add dst-address=192.168.23.0/24 gateway=192.168.255.1
/ip/route/ add dst-address=192.168.28.0/24 gateway=192.168.255.1

```

## Config R3
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R3
/ip/address add address=192.168.255.6/30 interface=ether4
/ip/address add address=192.168.210.1/24 interface=ether2

/ip/route/ add dst-address=192.168.0.0/16 gateway=192.168.255.5

```


## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
ip 192.168.28.2 255.255.255.0 192.168.28.1
```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC2
ip 192.168.146.2 255.255.255.0 192.168.146.1

```

## Config VPC 3
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC3
ip 192.168.210.2 255.255.255.0 192.168.210.1


```


## Config Eigener Laptop
In *cmd.exe* als Admin:
```cmd
route add 192.168.0.0 mask 255.255.0.0 192.168.23.137

```

## Quellen
 - Tim Schefer
 -Justin Barthel

## Neue Lerninhalte
 -Routing
 -Lokale Route auf auf einem Windows 10 PC konfigurieren
 -Routen mit Mikrotik Konsole hinzufügen

## Reflexion
Die Übung ist eine Erweiterung von LAB3, soll eigentlich einfach sein wenn ich LAB4 habe, trotzdem habe ich sehr viele Zeit verschwendet, weil ich statt ein Cloud ein NAT ausversehen genommen habe, bin sehr dankbar zu deren leute, wo mich geholfen hat.  
