# Dokumentation Übung - Labor mit zwei Router und DHCP Server

 - Datum: 04.02.2022
 - Name: Kaiwen Shao
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors]()

## Cloud
br0 192.168.23.0
Eigener PC ist via OpenVPN (Layer2) mit br0 verbunden.

## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 4 Interfaces Enabled
```
/ip address
add address=192.168.23.20/24 interface=ether1 network=192.168.23.0
add address=192.168.255.1/30 interface=ether3 network=192.168.255.0

/ip dhcp-client
add interface=ether1
add interface=ether2

/ip firewall nat
add action=masquerade chain=srcnat out-interface=ether2

/ip route
add dst-address=192.168.46.0/24 gateway=192.168.255.2

```
## Config R2
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/ip pool
add name=dhcp_pool0 ranges=192.168.46.10-192.168.46.254

/ip dhcp-server
add address-pool=dhcp_pool0 interface=ether1 name=dhcp1

/ip address
add address=192.168.46.1/24 interface=ether1 network=192.168.46.0
add address=192.168.23.21/24 interface=ether2 network=192.168.23.0
add address=192.168.255.2/30 interface=ether3 network=192.168.255.0

/ip dhcp-client
add interface=ether1

/ip dhcp-server network
add address=192.168.46.0/24 gateway=192.168.46.1

/ip route
add dst-address=0.0.0.0/0 gateway=192.168.255.1
```


## Quellen
    Rene Ottenburg
    https://help.mikrotik.com/

## Neue Lerninhalte
 - NAT aussetzen
 - Debian PC

## Reflexion
Am Anfang wusste ich nichts über NAT und Debian und weil ich frühner GNS3 noch falsch installiert habe, habe ich extrem lange uzeit verschwendet, aber jedoch mit der Hilfe von Tim Schefer und Rene Ottenburg ist es am ende einfacher geworden.

